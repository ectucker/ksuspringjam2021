extends Node


var levels = []
var current_level = -1


func _ready():
	register_levels()

func register_levels():
	levels.append("res://levels/level1.tscn")
	levels.append("res://levels/level1SLASH2.tscn")
	levels.append("res://levels/level2.tscn")
	levels.append("res://levels/basic basic.tscn")
	levels.append("res://levels/basic_level_4.tscn")
	levels.append("res://levels/basic_level.tscn")
	levels.append("res://levels/basic_level_2.tscn")
	levels.append("res://levels/basic_level_3.tscn")
	levels.append("res://levels/basic_level_5.tscn")
	levels.append("res://levels/basic_level_6.tscn")
	levels.append("res://levels/blockIntro.tscn")
	levels.append("res://levels/fruitIntro.tscn")
	levels.append("res://levels/complex_1.tscn")
	levels.append("res://levels/complex_2.tscn")
	levels.append("res://levels/complex_3.tscn")
	levels.append("res://levels/complex_4.tscn")
	levels.append("res://levels/buttonIntro.tscn")
	levels.append("res://levels/complex_5.tscn")
	levels.append("res://levels/complex_6.tscn")


func next_level():
	current_level += 1
	if current_level < levels.size():
		SolidRegistry.clear()
		GlobalEffects.spiral_to(levels[current_level])
	else:
		get_tree().paused = true
		GlobalEffects.destroy()

func restart_level():
	SolidRegistry.clear()
	GlobalEffects.spiral_to(levels[current_level])

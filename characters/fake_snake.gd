extends Node2D


var move_delay = 0.32
var next_move = move_delay


func _ready():
	reindex_segments()


func _process(delta):
	next_move -= delta
	if next_move < 0:
		next_move = move_delay
		var children = get_children()
		children.invert()
		for i in range(children.size()):
			var next = wrapi(i + 1, 0, children.size())
			children[i].move((children[next].global_position - children[i].global_position).normalized())
		reindex_segments()

func reindex_segments():
	for i in range(0, get_child_count()):
		get_child(i).segment_index = i
		get_child(i).num_segments = get_child_count()
		get_child(i).update_segment_stuff()

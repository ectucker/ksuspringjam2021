class_name SegmentCollider
extends KinematicBody2D


func get_snake():
	return get_parent().get_parent()

func get_segment():
	return get_parent()

extends Node2D


var tile_size: int = 64

onready var tween: Tween = $Tween

var segment_index = 0
var num_segments = 1

func _process(delta):
	update_segment_stuff()

func move(dir):
	tween.interpolate_property(self, "position",
		position, position + dir * tile_size,
		0.3, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()

func update_segment_stuff():
	if segment_index == 0:
		$Visible/Head.visible = true
		$Visible/Tail.visible = false
		$Visible/Sprite1.visible = false
		$Visible/Sprite2.visible = false
		var dir = global_position - get_parent().get_child(1).global_position
		$Visible.rotation = fmod(atan2(dir.y, dir.x) + PI / 2, TAU)
	elif segment_index % 2 == 0:
		$Visible/Head.visible = false
		$Visible/Tail.visible = false
		$Visible/Sprite1.visible = true
		$Visible/Sprite2.visible = false
	else:
		$Visible/Head.visible = false
		$Visible/Tail.visible = false
		$Visible/Sprite1.visible = false
		$Visible/Sprite2.visible = true
	
	var scale = ((1.0 - ((segment_index + 1.0) / num_segments)) * 0.5 + 0.5) * 0.25
	$Visible/Sprite1.scale = Vector2(scale, scale)
	$Visible/Sprite2.scale = Vector2(scale, scale)

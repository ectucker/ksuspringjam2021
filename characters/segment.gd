extends Node2D


var tile_size: int = 64

onready var ray: RayCast2D = $Body/Facing
onready var tween: Tween = $Tween

var segment_index = 0
var num_segments = 1

func _process(delta):
	update_segment_stuff()

func move(dir, check_collision):
	if check_collision:
		ray.cast_to = dir * tile_size
		ray.force_raycast_update()
		if ray.is_colliding():
			var collider = ray.get_collider()
			if collider is SegmentCollider:
				if collider.get_segment().segment_index != get_parent().get_child_count() - 1:
					return false
			else:
				return false
	
	tween.interpolate_property(self, "position",
		position, position + dir * tile_size,
		0.2, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	#tween.interpolate_property($Visible, "rotation",
	#	fmod($Visible.rotation, TAU), fmod(atan2(dir.y, dir.x) + PI / 2, TAU),
	#	0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	return true

func eat_animation(new_pos):
	tween.interpolate_property(self, "global_position",
		global_position, new_pos,
		0.4, Tween.TRANS_QUART, Tween.EASE_IN)
	tween.start()

func final_eat_animation(new_pos):
	tween.interpolate_property(self, "global_position",
		global_position, new_pos,
		0.4, Tween.TRANS_QUART, Tween.EASE_IN)
	tween.interpolate_property(self, "scale",
		scale, Vector2.ZERO,
		0.2, Tween.TRANS_QUART, Tween.EASE_IN, 0.2)
	tween.start()

func disapear_animation(new_pos):
	tween.interpolate_property(self, "global_position",
		global_position, new_pos,
		0.3, Tween.TRANS_QUART, Tween.EASE_IN)
	tween.interpolate_property(self, "scale",
		scale, Vector2.ZERO,
		0.3, Tween.TRANS_QUART, Tween.EASE_IN, 0.2)
	tween.interpolate_callback(self, 0.3, "remove")
	tween.start()

func update_segment_stuff():
	if segment_index == 0:
		$Visible/Head.visible = true
		$Visible/Tail.visible = false
		$Visible/Sprite1.visible = false
		$Visible/Sprite2.visible = false
		var dir = global_position - get_parent().get_child(1).global_position
		$Visible.rotation = fmod(atan2(dir.y, dir.x) + PI / 2, TAU)
	#elif segment_index == get_parent().get_child_count() - 1:
	#	$Visible/Head.visible = false
	#	$Visible/Tail.visible = true
	#	$Visible/Sprite1.visible = false
	#	$Visible/Sprite2.visible = false
	#	var dir = get_parent().get_child(segment_index - 1).global_position - global_position
	#	$Visible.rotation = fmod(atan2(dir.y, dir.x) + PI / 2, TAU)
	elif segment_index % 2 == 0:
		$Visible/Head.visible = false
		$Visible/Tail.visible = false
		$Visible/Sprite1.visible = true
		$Visible/Sprite2.visible = false
	else:
		$Visible/Head.visible = false
		$Visible/Tail.visible = false
		$Visible/Sprite1.visible = false
		$Visible/Sprite2.visible = true
	
	var scale = ((1.0 - ((segment_index + 1.0) / num_segments)) * 0.5 + 0.5) * 0.25
	$Visible/Sprite1.scale = Vector2(scale, scale)
	$Visible/Sprite2.scale = Vector2(scale, scale)

func remove():
	var tree = get_tree()
	get_parent().remove_child(self)
	queue_free()

func get_faced_segment():
	var collider = $Body/Facing.get_collider()
	if collider == null:
		return null
	return collider.get_parent()

func get_snake():
	return get_parent()

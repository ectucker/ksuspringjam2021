extends Node2D


var time = 0


func _process(delta):
	time += delta
	position.x = sin(time + get_parent().segment_index) * 3.0

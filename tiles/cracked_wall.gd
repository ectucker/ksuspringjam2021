extends Node2D


var x
var y

var destroyed = false


func _ready():
	var min_x = -10
	var max_x = 10
	var min_y = -10
	var max_y = 10
	var tile_size = 64
	
	x = int(global_position.x / tile_size) - min_x
	y = int(global_position.y / tile_size) - min_y
	SolidRegistry.register_breakable(self, x, y)

func destroy():
	if not destroyed:
		destroyed = true
		$Collision.collision_layer = 0
		$Collision.collision_mask = 0
		visible = false
		GlobalSounds.play_if_not("RockBreak")

extends Node2D


var num_bodies = 0
var pressed_last = false
var pressed = false


func _ready():
	SolidRegistry.register_button(self)
	$Collision.connect("body_entered", self, "body_entered")
	$Collision.connect("body_exited", self, "body_exited")

func _process(delta):
	if pressed and not pressed_last:
		GlobalSounds.play_if_not("Button")
	pressed_last = pressed

func body_entered(body):
	if body is SegmentCollider:
		num_bodies += 1
	pressed = num_bodies > 0
	$SpriteUp.visible = not pressed
	$SpriteDown.visible = pressed

func body_exited(body):
	if body is SegmentCollider:
		num_bodies -= 1
	pressed = num_bodies > 0
	$SpriteUp.visible = not pressed
	$SpriteDown.visible = pressed

extends Node2D


var x
var y


func _ready():
	var min_x = -10
	var max_x = 10
	var min_y = -10
	var max_y = 10
	var tile_size = 64
	
	x = int(global_position.x / tile_size) - min_x
	y = int(global_position.y / tile_size) - min_y
	SolidRegistry.register_solid(x, y)

func _process(delta):
	if SolidRegistry.all_buttons_pressed():
		if $ClosedSprite.visible:
			GlobalSounds.play_if_not("GateOpen")
		set_solid(false)
		SolidRegistry.doors_open = true
	elif not SolidRegistry.doors_open:
		set_solid(true)

func set_solid(solid):
	if solid:
		SolidRegistry.register_solid(x, y)
		$Collision.set_collision_layer_bit(0, 1)
		$Collision.set_collision_mask_bit(0, 1)
		$ClosedSprite.visible = true
		$OpenSprite.visible = false
	else:
		SolidRegistry.register_empty(x, y)
		$Collision.set_collision_layer_bit(0, 0)
		$Collision.set_collision_mask_bit(0, 0)
		$ClosedSprite.visible = false
		$OpenSprite.visible = true

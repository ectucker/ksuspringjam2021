extends Node2D


func _ready():
	$Collision.connect("body_entered", self, "body_entered")
	SolidRegistry.register_fruit(self)

func body_entered(body):
	if body is SegmentCollider:
		body.get_snake().eat_fruit()
		GlobalSounds.play_if_not("Fruit")
		queue_free()

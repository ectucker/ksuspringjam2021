extends Node2D


func _ready():
	var min_x = -10
	var max_x = 10
	var min_y = -10
	var max_y = 10
	var tile_size = 64
	
	var x = int(global_position.x / tile_size)
	var y = int(global_position.y / tile_size)
	SolidRegistry.register_solid(x - min_x, y - min_y)

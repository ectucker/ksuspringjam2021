extends Node2D


var tile_size: int = 64

var segment_scene = preload("res://characters/segment.tscn")

const MOVE_DELAY = 0.22
var move_time = 0.0
var move_dir = Vector2.ZERO

var next_hiss_time = 5.0

var pieces_to_add = 0

var win_callback: FuncRef = null

var tongue_out = false

var undo_stack = []


func _ready():
	reindex_segments()

func _process(delta):
	move_time -= delta
	
	if move_time < 0:
		if win_callback != null:
			win_callback.call_func()
			win_callback = null
		
		if move_dir == Vector2.UP and Input.is_action_pressed("move_up"):
			if move(move_dir):
				move_time = MOVE_DELAY
		elif move_dir == Vector2.DOWN and Input.is_action_pressed("move_down"):
			if move(move_dir):
				move_time = MOVE_DELAY
		elif move_dir == Vector2.LEFT and Input.is_action_pressed("move_left"):
			if move(move_dir):
				move_time = MOVE_DELAY
		elif move_dir == Vector2.RIGHT and Input.is_action_pressed("move_right"):
			if move(move_dir):
				move_time = MOVE_DELAY
		
		if get_child(0).get_faced_segment() == get_child(get_child_count() - 1):
			if not tongue_out:
				get_child(0).get_node("AnimationPlayer").play("tongue_out")
			tongue_out = true
		else:
			if tongue_out:
				get_child(0).get_node("AnimationPlayer").play("tongue_in")
			tongue_out = false
	
	next_hiss_time -= delta
	if next_hiss_time < 0:
		next_hiss_time = rand_range(5.0, 9.0)
		GlobalSounds.play_one(["Idle1", "Idle2", "Idle3"])

func _input(event):
	if true:
		if event.is_action_pressed("move_up"):
			move_dir = Vector2.UP
			if move_time < 0 and move(Vector2.UP):
				move_time = MOVE_DELAY
		if event.is_action_pressed("move_down"):
			move_dir = Vector2.DOWN
			if move_time < 0 and move(Vector2.DOWN):
				move_time = MOVE_DELAY
		if event.is_action_pressed("move_left"):
			move_dir = Vector2.LEFT
			if move_time < 0 and move(Vector2.LEFT):
				move_time = MOVE_DELAY
		if event.is_action_pressed("move_right"):
			move_dir = Vector2.RIGHT
			if move_time < 0 and move(Vector2.RIGHT):
				move_time = MOVE_DELAY
	if move_time < 0.0:
		if event.is_action_pressed("action_eat"):
			if get_child(0).get_faced_segment() == get_child(get_child_count() - 1):
				eat_self()
				move_time = 0.4
	
	if event.is_action_pressed("level_restart"):
		LevelManager.restart_level()
	
	if event.is_action_pressed("level_skip"):
		LevelManager.next_level()
	
	if event.is_action_pressed("action_undo"):
		if move_time < 0.0:
			if undo_stack.size() > 0:
				var state = undo_stack.pop_back()
				for i in range(0, state.size() - 1):
					get_child(i).global_position = state[i]
				SolidRegistry.doors_open = state[state.size() - 1]
				SolidRegistry.force_doors = SolidRegistry.doors_open
				SolidRegistry.force_door_time = 0.1

func move(dir):
	var tail_pos = get_child(get_child_count() - 1).global_position
	var segment = get_child(0)
	var last_pos = segment.position
	if segment.move(dir, true):
		for i in range(1, get_child_count()):
			segment = get_child(i)
			dir = (last_pos - segment.position).normalized()
			last_pos = segment.position
			segment.move(dir, false)
		# Add pieces if needed
		if pieces_to_add > 0:
			pieces_to_add -= 1
			var new_segment = segment_scene.instance()
			add_child(new_segment)
			new_segment.global_position = tail_pos
			undo_stack.clear()
		reindex_segments()
		GlobalSounds.play_if_not("Move")
		
		var state = []
		for i in range(get_child_count()):
			state.append(get_child(i).global_position)
		state.append(SolidRegistry.doors_open)
		undo_stack.push_back(state)
		
		return true
	return false

func reindex_segments():
	for i in range(0, get_child_count()):
		get_child(i).segment_index = i
		get_child(i).num_segments = get_child_count()
		get_child(i).update_segment_stuff()

func eat_fruit():
	pieces_to_add += 2
	undo_stack.clear()

func eat_self():
	var result = get_contained_tiles()
	
	if result == null:
		return
	if result.size() == get_child_count():
		return
	
	GlobalSounds.play_if_not("Eat")
	
	# Ending eating
	if result.size() == 0:
		if SolidRegistry.all_fruits_eaten():
			win_callback = funcref(GlobalMenus, "show_win_menu")
		else:
			win_callback = funcref(GlobalMenus, "show_lose_menu")
		var center = find_center()
		for child in get_children():
			child.final_eat_animation(center)
	# Partial eating
	else:
		# For each node that's moving
		for i in range(result.size()):
			var target = get_child(i)
			var target_pos = (result[i] - Vector2(10, 10)) * 64
			target.eat_animation(target_pos)
		# For each node we're eating
		var final_head_pos = (result[0] - Vector2(10, 10)) * 64
		for i in range(result.size(), get_child_count()):
			get_child(i).disapear_animation(final_head_pos)
		undo_stack.clear()
	
	reindex_segments()

func find_center():
	var x_tot = 0.0
	var y_tot = 0.0
	for child in get_children():
		x_tot += child.global_position.x
		y_tot += child.global_position.y
	return Vector2(x_tot / get_child_count(), y_tot / get_child_count())

func get_contained_tiles():
	var min_x = -10
	var max_x = 10
	var min_y = -10
	var max_y = 10
	
	var width = max_x - min_x
	var height = max_y - min_y
	
	var cols = []
	for y in range(0, height):
		var row = []
		for x in range(0, width):
			row.append(0)
		cols.append(row)
	
	var grid = Array2D.new(cols)
	
	for child in get_children():
		var x = int(child.global_position.x / tile_size)
		var y = int(child.global_position.y / tile_size)
		grid.set_cell(x - min_x, y - min_y, 1)
	
	var area_counts = {}
	var start_points = {}
	var sectors = 2
	for i in range(0, width):
		for j in range(0, height):
			if grid.get_cell(i, j) == 0:
				area_counts[sectors] = flood_fill(grid, i, j, sectors)
				start_points[sectors] = Vector2(i, j)
				sectors += 1
	
	var max_region = area_counts.keys()[0]
	for key in area_counts.keys():
		if area_counts[key] > area_counts[max_region]:
			max_region = key
	
	var min_regions = []
	for key in area_counts.keys():
		if key != max_region:
			min_regions.append(key)
	
	# If there isn't an actually enclosed area
	if min_regions.size() == 0:
		return []
	if area_counts[min_regions[0]] > 30:
		return []
	
	for i in range(0, width):
		for j in range(0, height):
			var cell = grid.get_cell(i, j)
			if min_regions.has(cell):
				if SolidRegistry.is_solid(i, j):
					grid.set_cell(i, j, 10)
				if SolidRegistry.is_breakable(i, j):
					SolidRegistry.break_tile(i, j)
	
	var shape_counts = {}
	var shape_start_points = {}
	var shape_ids = 11
	for i in range(0, width):
		for j in range(0, height):
			if grid.get_cell(i, j) == 10:
				shape_counts[shape_ids] = flood_fill(grid, i, j, shape_ids, 10)
				shape_start_points[shape_ids] = Vector2(i, j)
				shape_ids += 1
	
	# If no shapes were found
	if shape_ids == 11:
		return [] # Victory!
	
	# If there are multiple shapes
	if shape_ids > 13:
		print("Please reconsider this level design")
	elif shape_ids > 12:
		var min_shape_dist = INF
		var min_shape1 = Vector2.ZERO
		var min_shape2 = Vector2.ZERO
		var shape1 = shape_counts.keys()[0]
		var shape2 = shape_counts.keys()[1]
		# Identify the minimum distance between the two points
		for i in range(0, width):
			for j in range(0, height):
				if grid.get_cell(i, j) == shape1:
					for l in range(0, width):
						for k in range(0, height):
							if grid.get_cell(l, k) == shape2:
								var dist = Vector2(l - i, k - j).length_squared()
								if dist < min_shape_dist:
									min_shape_dist = dist
									min_shape1 = Vector2(i, j)
									min_shape2 = Vector2(l, k)
		var dir = (min_shape2 - min_shape1).normalized()
		var steps = sqrt(min_shape_dist)
		for i in range(0, steps):
			grid.set_cellv(min_shape1, 10)
			min_shape1 += dir
	
	var snake_cells = []
	
	for i in range(0, width):
		for j in range(0, height):
			if grid.get_cell(i, j) < 10:
				if check_location_safe(grid, i + 1, j) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i - 1, j) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i, j + 1) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i, j - 1) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i + 1, j + 1) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i + 1, j - 1) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i - 1, j + 1) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
				if check_location_safe(grid, i - 1, j - 1) >= 10:
					grid.set_cell(i, j, 9)
					snake_cells.push_back(Vector2(i, j))
	
	var head_pos = (get_child(0).global_position / 64) + Vector2(10, 10)
	
	var min_dist = INF
	var min_x_head = 0
	var min_y_head = 0
	for i in range(0, width):
		for j in range(0, height):
			if grid.get_cell(i, j) == 9:
				var dist = Vector2(head_pos.x - i, head_pos.y - j).length_squared()
				if dist < min_dist:
					min_x_head = i
					min_y_head = j
					min_dist = dist
	
	var decision_points = []
	for cell in snake_cells:
		var paths = 0
		if check_location_safev(grid, cell + Vector2.RIGHT) == 9:
			paths += 1
		if check_location_safev(grid, cell + Vector2.LEFT) == 9:
			paths += 1
		if check_location_safev(grid, cell + Vector2.UP) == 9:
			paths += 1
		if check_location_safev(grid, cell + Vector2.DOWN) == 9:
			paths += 1
		if paths >= 3:
			if not decision_points.has(cell):
				decision_points.append(cell)
	
	grid.set_cell(min_x_head, min_y_head, 0)
	
	var snake_list = []
	flood_fill_create(grid, min_x_head, min_y_head, snake_list, decision_points)
	
	if snake_list.size() > get_child_count():
		return null
	
	var average_dist_forward = 0
	var average_dist_back = 0
	for i in range(snake_list.size()):
		var target = get_child(i)
		var target_pos1 = (snake_list[i] - Vector2(10, 10)) * 64
		var target_pos2 = (snake_list[snake_list.size() - i - 1] - Vector2(10, 10)) * 64
		average_dist_forward += (target.global_position - target_pos1).length_squared()
		average_dist_back += (target.global_position - target_pos2).length_squared()
	
	if average_dist_back < average_dist_forward:
		snake_list.invert()
	
	return snake_list

func check_location_safe(grid, x, y):
	if x >= 20:
		return -1
	if x < 0:
		return -1
	if y >= 20:
		return -1
	if y < 0:
		return -1
	return grid.get_cell(x, y)

func check_location_safev(grid, vec):
	if vec.x >= 20:
		return -1
	if vec.x < 0:
		return -1
	if vec.y >= 20:
		return -1
	if vec.y < 0:
		return -1
	return grid.get_cellv(vec)

func flood_fill_create(grid, x, y, snake_list, decision_points):
	var queue = []
	queue.push_back(Vector2(x, y))
	while queue.size() > 0:
		var location = queue.pop_back()
		grid.set_cellv(location, -9)
		snake_list.push_back(location)
		
		if location.x + 1 < 20 and grid.get_cellv(location + Vector2.RIGHT) == 9:
			var new_loc = location + Vector2.RIGHT
			# If at least one point is not a decision point
			if not decision_points.has(new_loc) or not decision_points.has(location):
				if not queue.has(new_loc):
					queue.push_back(new_loc)
		if location.x - 1 > 0 and grid.get_cellv(location + Vector2.LEFT) == 9:
			var new_loc = location + Vector2.LEFT
			# If at least one point is not a decision point
			if not decision_points.has(new_loc) or not decision_points.has(location):
				if not queue.has(new_loc):
					queue.push_back(new_loc)
		if location.y - 1 > 0 and grid.get_cellv(location + Vector2.UP) == 9:
			var new_loc = location + Vector2.UP
			# If at least one point is not a decision point
			if not decision_points.has(new_loc) or not decision_points.has(location):
				if not queue.has(new_loc):
					queue.push_back(new_loc)
		if location.y + 1 < 20 and grid.get_cellv(location + Vector2.DOWN) == 9:
			var new_loc = location + Vector2.DOWN
			# If at least one point is not a decision point
			if not decision_points.has(new_loc) or not decision_points.has(location):
				if not queue.has(new_loc):
					queue.push_back(new_loc)

func flood_fill(grid, x, y, value, empty_value = 0):
	var queue = []
	var num_set = 0
	queue.push_back(Vector2(x, y))
	while queue.size() > 0:
		var location = queue.pop_back()
		if grid.get_cellv(location) != value:
			grid.set_cellv(location, value)
			num_set += 1
		
		if check_location_safev(grid, location + Vector2.RIGHT) == empty_value:
			queue.push_back(location + Vector2.RIGHT)
		if check_location_safev(grid, location + Vector2.LEFT) == empty_value:
			queue.push_back(location + Vector2.LEFT)
		if check_location_safev(grid, location + Vector2.UP) == empty_value:
			queue.push_back(location + Vector2.UP)
		if check_location_safev(grid, location + Vector2.DOWN) == empty_value:
			queue.push_back(location + Vector2.DOWN)
		
		if check_location_safev(grid, location + Vector2.RIGHT + Vector2.UP) == empty_value:
			queue.push_back(location + Vector2.RIGHT + Vector2.UP)
		if check_location_safev(grid, location + Vector2.RIGHT + Vector2.DOWN) == empty_value:
			queue.push_back(location + Vector2.RIGHT + Vector2.DOWN)
		if check_location_safev(grid, location + Vector2.LEFT + Vector2.UP) == empty_value:
			queue.push_back(location + Vector2.LEFT + Vector2.UP)
		if check_location_safev(grid, location + Vector2.LEFT + Vector2.DOWN) == empty_value:
			queue.push_back(location + Vector2.LEFT + Vector2.DOWN)
	return num_set

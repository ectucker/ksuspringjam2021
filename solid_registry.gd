extends Node


var grid

var buttons = []
var fruits = []
var breakables = []
var doors_open = false

var force_doors = false
var force_door_time = 0.0


func _ready():
	var min_x = -10
	var max_x = 10
	var min_y = -10
	var max_y = 10
	
	var width = max_x - min_x
	var height = max_y - min_y
	
	var cols = []
	for y in range(0, height):
		var row = []
		for x in range(0, width):
			row.append(0)
		cols.append(row)
	
	grid = Array2D.new(cols)

func _process(delta):
	force_door_time -= delta
	if force_door_time > 0:
		doors_open = force_doors

func clear():
	for y in range(0, 20):
		for x in range(0, 20):
			grid.set_cell(x, y, 0)
	buttons.clear()
	breakables.clear()
	fruits.clear()
	doors_open = false

func register_solid(x, y):
	grid.set_cell(x, y, 1)

func register_empty(x, y):
	grid.set_cell(x, y, 0)

func register_button(button):
	buttons.append(button)

func register_fruit(fruit):
	fruits.append(weakref(fruit))

func register_breakable(breakable, x, y):
	grid.set_cell(x, y, 3)
	breakables.append(breakable)

func is_solid(x, y):
	return grid.get_cell(x, y) == 1

func is_breakable(x, y):
	return grid.get_cell(x, y) == 3

func break_tile(x, y):
	for tile in breakables:
		if tile.x == x:
			if tile.y == y:
				tile.destroy()

func all_buttons_pressed():
	for button in buttons:
		if not button.pressed:
			return false
	return true

func all_fruits_eaten():
	for fruit in fruits:
		if fruit.get_ref():
			return false
	return true

extends Node


func play_if_not(sound):
	var player: AudioStreamPlayer = get_node(sound)
	if not player.playing:
		player.play()

func play_one(sounds):
	var i = randi() % sounds.size()
	get_node(sounds[i]).play()

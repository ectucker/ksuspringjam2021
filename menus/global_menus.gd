extends Node


func show_win_menu():
	GlobalSounds.play_if_not("Victory")
	get_tree().paused = true
	$WinMenu/Stack/Details.text = "Finished " + str(LevelManager.current_level + 1) + "/" + str(LevelManager.levels.size())
	$WinMenu.visible = true

func show_lose_menu():
	get_tree().paused = true
	$LoseMenu.visible = true

func hide_all():
	get_tree().paused = false
	for child in get_children():
		child.visible = false

func _input(event):
	if not event.is_action_pressed("dev_fullscreen"):
		if event is InputEventKey:
			if event.is_pressed():
				if $WinMenu.visible:
					next_level()
	
	if event.is_action("level_restart"):
		if $LoseMenu.visible:
			restart_level()

func restart_level():
	LevelManager.restart_level()

func next_level():
	LevelManager.next_level()

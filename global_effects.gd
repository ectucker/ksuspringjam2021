extends CanvasLayer


onready var tween = $Tween


func spiral_to(next_scene):
	get_tree().paused = true
	$AnimationPlayer.play("spiral_out")
	yield($AnimationPlayer, "animation_finished")
	GlobalMenus.hide_all()
	get_tree().change_scene(next_scene)
	$AnimationPlayer.play("spiral_in")
	yield($AnimationPlayer, "animation_finished")
	get_tree().paused = false

func destroy():
	GlobalMenus.hide_all()
	get_tree().paused = true
	tween.interpolate_property(get_tree().current_scene, "scale",
		Vector2(1.0, 1.0), Vector2(0.0, 0.0),
		5.0, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.interpolate_callback(self, 10.0, "return_to_main")
	tween.start()

func return_to_main():
	LevelManager.current_level = -1
	spiral_to("res://main.tscn")

func _input(event):
	if event.is_action_pressed("dev_fullscreen"):
		if OS.get_name() != "HTML5":
			OS.window_fullscreen = !OS.window_fullscreen
